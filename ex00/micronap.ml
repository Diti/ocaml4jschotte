(*
   Package Unix is needed to compile this example. Compile with:
       ocamlfind ocamlopt -package Unix -linkpkg micronap.ml
*)
let my_sleep () = Unix.sleep 1

let sleep_for_seconds n =
    for i = 1 to n do
        my_sleep () ; ignore i
    done

let () =
    let args_num = Array.length (Sys.argv) - 1 in
    if args_num = 1 then
    begin
        try
            int_of_string Sys.argv.(1) |> sleep_for_seconds
        with
        | Failure _ -> ()
        | _ -> ()
    end
