(*
   http://blog.enfranchisedmind.com/2007/08/a-monad-tutorial-for-ocaml/

   “What is a monad? At the most simplistic level, it is simply any
   data structure or type which fullfills (or can fullfill)
   the following signature:

       module type MonadRequirements = sig
           type ‘a t
           val bind : ‘a t -> (‘a -> ‘b t) -> ‘b t
           val return : ‘a -> ‘a t
        end

    In other words, a monad is a box- you can put things into the box
    (with return) and manipulate things inside of the box (with bind).”
*)

type 'a ft_ref = { mutable cont: 'a }

let return =
  fun r -> { cont = r }

let get =
  fun r -> r.cont

let set =
  fun r v -> r.cont <- v

let bind:'a ft_ref -> ('a -> 'b ft_ref) -> 'b ft_ref =
  fun x f -> f (get x)

let () =
  let change_me = return "Change me!" in
  return 42                |> get |> Printf.printf "            return 42 |> get --> %d\n" ;
  return "Hello world!"    |> get |> Printf.printf "return \"Hello world!\" |> get --> \"%s\"\n" ;

  change_me                |> get |> Printf.printf "            change_me |> get --> \"%s\"\n" ;
  set change_me "Changed!" ;
  change_me                |> get |> Printf.printf "            change_me |> get --> \"%s\"\n" ;

  bind change_me (fun str -> return (str ^ " Using bind, and a function!"))
                           |> get |> Printf.printf "            change_me |> get --> \"%s\"\n" ;
