let jokes = [|
"Q. What do penguins eat for lunch?
A. Ice burg-ers.";

"Q. What do you call a penguin in a desert?
A. Lost!";

"Q. What’s black and white and very, very dangerous?
A. A penguin with the root password.";

"Q. Why are penguins so good racing drivers?
A. Because they are always in pole position.";

"Q. How does a group of penguins make a decision?
A. Flipper coin.";
|]

let () =
    Random.self_init () ;

    Random.int (Array.length jokes)
    |> Array.get jokes
    |> print_endline

