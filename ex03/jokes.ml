let process_file_jokes_as_array filename =
  let joke_file = open_in filename in
  let joke_arr = ref [||] in
  let joke_list = ref [] in
  begin try
      let joke_str = ref "" in
      while true do
        let line = input_line joke_file in
        if line <> "%" then
          ignore (joke_str := !joke_str ^ line ^ "\n")
        else
          begin
            joke_list := !joke_list @ [!joke_str];
            joke_arr := Array.copy (Array.of_list !joke_list);
            joke_str := ""
          end
      done
    with
    | End_of_file -> ()
  end;
  close_in joke_file;
  !joke_arr

let () =
  if Array.length Sys.argv <> 2 then
    Printf.fprintf Pervasives.stderr "Usage: %s jokes_file\n" Sys.argv.(0)
  else
  begin
    Random.self_init () ;

    let jokes = Sys.argv.(1) |> process_file_jokes_as_array in
    Random.int (Array.length jokes)
    |> Array.get jokes
    |> print_string
  end

