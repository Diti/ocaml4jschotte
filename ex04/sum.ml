let sum =
  fun a b -> a +. b

let try_with x y =
  sum x y |> Printf.printf "%10f +. %10f = %10f\n" x y

let () =
  if Array.length Sys.argv = 3 then
    try
      try_with (float_of_string Sys.argv.(1)) (float_of_string Sys.argv.(2))
    with
    | Failure err -> Printf.fprintf Pervasives.stderr "Input error (%s)\n" err
  else
    begin
      prerr_endline "Hint: pass two float parameters to this program to compute their sum.";
      try_with (-42.) (-42.);
      try_with 0. 0.;
      try_with 42. 42.;
      try_with 999999999999999999.9 42.;
      try_with max_float max_float;
      try_with max_float 42.;
      try_with infinity infinity;
      try_with infinity 42.;
      try_with neg_infinity 42.;
      try_with neg_infinity infinity;
      try_with infinity nan;
      try_with 42. nan;
    end
